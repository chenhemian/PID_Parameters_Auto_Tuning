function [num,den] = GouZaotf(sys1,num0,den0)
%构造系统传递函数，并返回系统系数
   sys2=tf(num0,den0);                %已知传递函数
   sys=sys1*sys2;                     %待整定传递函数
   sys=feedback(sys,+1);              %构造闭环传递函数
   [num,den]=tfdata(sys);             %待整定传递函数系数
end

